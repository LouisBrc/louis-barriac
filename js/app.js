//===================  Chargement de la page
const loader = document.querySelector('.loader');
window.addEventListener("load",() => {
    loader.classList.add('endload');
});

//===================  Dark mode
const switchMode = document.getElementById("switch");

switchMode.addEventListener("click", () => {
  if (switchMode.checked) {
    document.documentElement.style.setProperty('--text', '#0D1117');
    document.documentElement.style.setProperty('--background', 'white');
    document.documentElement.style.setProperty('--secondary-bg', '#e9ecef');
    document.getElementById('notion').src="/img/tools/notion_black.png";
    document.querySelector('.icon1').style.display = 'none';
    document.querySelector('.icon2').style.display = 'block';
  } else {
    document.documentElement.style.setProperty('--text', 'white');
    document.documentElement.style.setProperty('--background', '#0D1117');
    document.documentElement.style.setProperty('--secondary-bg', '#161B22');
    document.getElementById('notion').src="/img/tools/notion.png";
    document.querySelector('.icon1').style.display = 'block';
    document.querySelector('.icon2').style.display = 'none';
  }
});


//=================== Vérifie si le logo est survolé
const logo = document.querySelectorAll('.tools .tools-grid img');
const desc = document.querySelectorAll('.tools .tools-grid p');

for (let i = 0; i < logo.length; i++) {
    logo[i].addEventListener('mouseenter', () => {
        desc[i].classList.add('hovered');
        desc[i].style.opacity = '1';
    });
    logo[i].addEventListener('mouseleave', () => {
        desc[i].classList.remove('hovered');
        desc[i].style.opacity = '0';
        desc[i].style.transition = 'all 0.25s ease-in';
    });
}


//=================== Apparition de la barre de navigation et animation du footer
const nav = document.querySelector('nav');
const footer = document.querySelector('footer');
const isMobile = window.getComputedStyle(document.querySelector('.mobile'))

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  //=================== Faire apparaître la navbar
  if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
    nav.classList.add('visible');
  } else {
    nav.classList.remove('visible');
  }


  //=================== Animation du footer
  if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && isMobile.display != 'flex') {
    party.sparkles(footer, {
        count: party.variation.range(25,50)
    })};

  //=================== Faire apparaitre le footer
  if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && isMobile.display === 'flex') {
        nav.classList.remove('visible');
  }
}

//Faire apparaitre le bouton scroll to top
const btnTop = document.querySelector(".to-top");

document.body.addEventListener('wheel', checkScrollDirection);

function checkScrollDirection(event) {
  if (checkScrollDirectionIsUp(event) && window.getComputedStyle(nav).opacity === '1') {
    btnTop.style.opacity = "1";
    btnTop.style.visibility = "visible";
    btnTop.style.transform= "translateY(0px)";
  } else {
    btnTop.style.opacity = "0";
    btnTop.style.visibility = "hidden";
    btnTop.style.transform= "translateY(10px)";
  }
}

function checkScrollDirectionIsUp(event) {
  if (event.wheelDelta) {
    return event.wheelDelta > 0;
  }
  return event.deltaY < 0;
}


const callback = function (entries) {
    entries.forEach(function (entry) {
      if (entry.isIntersecting) {
        console.log('hey')
        activate(entry.target)
      }
    })
}

const y = Math.round(window.innerHeight * 0.6)
const observer = new IntersectionObserver(callback, {
  rootMargin: `-${window.innerHeight - y - 1}px 0px -${y}px 0px`
})


const activate = function (elem) {
    const id = elem.getAttribute('id')
    const anchor = document.querySelector(`a[href="#${id}"]`)
    if (anchor === null) {
      return null
    }
    anchor.parentElement
      .querySelectorAll('.active')
      .forEach(node => node.classList.remove('active'))
    anchor.classList.add('active')
}

// =================== Apparition au scroll
// const options = {
//   root: null,
//   rootMargin: '0px',
//   threshold: .1
// }

// const handleIntersect = function(entries, observer) {
//   entries.forEach(function(entry) {
//     if (entry.intersectionRatio > .1) {
//       entry.target.classList.add('reveal-visible')
//       observer.unobserve(entry.target)
//     }
//   })
// }

// const observateur = new IntersectionObserver(handleIntersect, options);
// document.querySelectorAll('[class*="reveal-"]').forEach(function(elem) {
//   observateur.observe(elem)
// })


let sr = ScrollReveal({
  useDelay: 'onload',
  easing: 'ease-out',
  origin: 'top',
  duration: 1000,
  viewOffset: {bottom: 100}
})

const animTop = {
  distance: '50px',    
  delay: 0,
}

const animBottom = {
  origin: 'bottom',
  distance: '50px',
  delay: 500
}


ScrollReveal().reveal('.title, .more, .toggle-btn', animTop);

ScrollReveal().reveal('.title-container .subtitle', {
  distance: '50px',
  duration: 800,
  delay: 300
});

ScrollReveal().reveal('.img', animBottom);

animBottom.distance ='100px'
ScrollReveal().reveal('.download-container', animBottom);

ScrollReveal().reveal('#elem', {
  interval: 200,
  origin: 'bottom',
  distance: '50px',
})


ScrollReveal().reveal('.experience-container', {
  origin: 'top'
})

ScrollReveal().reveal('.tools-grid .title', {
  distance: '0'
})


// ======= Mobile
if (isMobile.display === 'flex') {
  ScrollReveal().reveal('.img', animTop);
  ScrollReveal().reveal('.title-container .title', {
    origin: 'bottom',
  });
  ScrollReveal().reveal('.title-container .subtitle', {
    origin: 'bottom',
    delay: 300,
    distance: '50px'
  });
  ScrollReveal().reveal('.download-container', {
    origin: 'bottom',
    distance: '100px',
    duration: 800,
    delay: 300
  });

  sr = ScrollReveal({
    easing: 'ease-out',
    origin: 'top',
    duration: 1000,
    viewOffset: {bottom: 200}
  })

}

if (isMobile.display === 'flex') {
  smoothScroll.defaults({duration: 500});
} else {
  smoothScroll.defaults({duration: 150});
}

document.querySelector('#nav-1').addEventListener('click', function(e){
  smoothScroll({toElement: document.getElementById('main')});
});
document.querySelector('#nav-2').addEventListener('click', function(e){
  smoothScroll({toElement: document.getElementById('about')});
});
document.querySelector('#nav-3').addEventListener('click', function(e){
  smoothScroll({toElement: document.getElementById('work')});
});

